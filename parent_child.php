<?php include "include.php";

class Person{
    public $name = "";
    public $age = "";

    public function __construct($nam,$ag)
    {
        $this->name =$nam;
        $this->age =$ag;
    }
    public function personDetails(){
        echo "My name is $this->name and my age is $this->age <br >";

    }
    // End of parent class
}
//start with child class
class people extends person{
        public $hobby;

    function myhobby(){
        echo "My hobby is $this->hobby <br>";
    }
}
$parent_obj = new Person("Istiyak","17");
$parent_obj->personDetails();
$child_obj = new people("Amin","18");
$child_obj->personDetails();
$child_obj->hobby = "Programming";
$child_obj->myhobby();


?>