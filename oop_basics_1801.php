<?php
class BITM{
    public $window;
    public $door;
    public $chair;
    public $table;
    public $whitebored;


    public function coolTheAir(){
        echo "I am the air cooler";
    }
    public function compute(){
        echo "I am computing";
    }

    public function show(){
        echo $this->window."<br>";
        echo $this->door."<br>";
        echo $this->table."<br>";
        echo $this->chair."<br>";
        echo $this->whitebored."<br>";
    }

    public function setWindow($window)
    {
        $this->window = $window;
    }

    public function setDoor($door)
    {
        $this->door = $door;
    }

    public function setChair($chair)
    {
        $this->chair = $chair;
    }
    public function setTable($table)
    {
        $this->table = $table;
    }

    public function setWhitebored($whitebored)
    {
        $this->whitebored = $whitebored;
    }
    public function parentDosomething(){
        echo "this line for child";
    }
  /*  public function setAll(){
        $this->setChair("I'm a chair");
        $this->setDoor("I'm a Door");
        $this->setTable("I'm Table");
        $this->setWhitebored("I'm Whitebored");
        $this->setWindow("I'm Window");
    }*/
}

$obj_BITM_at_ctg = new BITM;

$obj_BITM_at_ctg->setChair("I'm a chair");
$obj_BITM_at_ctg->setDoor("I'm a Door");
$obj_BITM_at_ctg->setTable("I'm Table");
$obj_BITM_at_ctg->setWhitebored("I'm Whitebored");
$obj_BITM_at_ctg->setWindow("I'm Window");
//  $obj_BITM_at_ctg->setAll();

$obj_BITM_at_ctg->show();

class BITM_Lab402 extends BITM{
    public $childchair;
    public function childDoSomething(){
        parent::parentDoSomething();
        echo "<br> this line for child";
    }
}

$obj_BITM_Lab402= new BITM_Lab402();
$obj_BITM_Lab402->setWindow("this is window");
$obj_BITM_Lab402->setWhitebored("this is white borad");
$obj_BITM_Lab402->setDoor("this is door ");
$obj_BITM_Lab402->setChair("this is chair");
$obj_BITM_Lab402->setTable("this is tsble");




$obj_BITM_Lab402->show();
$obj_BITM_Lab402->childDoSomething();
?>