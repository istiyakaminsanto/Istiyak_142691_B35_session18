<?php include "include.php"; ?>

<?php

class Person{
    public $id;
    const NAME ="Istiyak Amin Santo";
    public static $brother ="Pranto";


    function __destruct()
    {
        echo "Good Bye!!";
    }
    //using construct
    public $name = "";
    public $age = "";

    public function __construct($nam,$ag)
    {
        $this->name =$nam;
        $this->age =$ag;
    }
    public function personDetails(){
        echo "My name is $this->name <br> and my age is $this->age <br> ";

    }
    public function display(){
        echo "My name is ".person::NAME."<br>";
        echo "My brother name is ".self::$brother."<br>";
    }
}

$obj = new person("Istiyak Amin Santo","17");
$obj->personDetails();
$obj->display();
?>